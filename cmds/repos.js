export default {
  name: "repos",
  aliases: ["repo", "projects", "project", "repository"],
  description: "Explore my repositories.",
  execute(print, subCmd, toTitleCase) {
    const platforms = {
      github: "https://github.com/LakhindarPal",
      gitlab: "https://gitlab.com/LakhindarPal",
      replit: "https://replit.com/@LakhindarPal",
      glitch: "https://glitch.com/@LakhindarPal",
      codepen: "https://codepen.io/lakhindarpal",
      codesandbox: "https://codesandbox.io/u/LakhindarPal",
      npm: "https://www.npmjs.com/~lakhindarpal"
    };

    let text = `<p>Discover my projects on:</p>`;

    if (subCmd) {
      const platform = platforms[subCmd];
      if (platform) {
        text += `<a href="${platform}">${toTitleCase(subCmd)}</a>`;
      } else {
        text = `<p>Invalid sub-command. Available sub-commands: ${Object.keys(platforms).join(', ')}</p>`;
      }
    } else {
      // Display all links
      text += Object.keys(platforms)
        .map(key => `<a href="${platforms[key]}">${toTitleCase(key)}</a>`)
        .join(" ");
    }

    print(text);
  }
};