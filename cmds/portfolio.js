export default {
  name: "portfolio",
  aliases: ["work", "my-work", "my-works", "showcase"],
  description: "Highlighted projects in the portfolio.",
  execute(print) {
    const projects = [
      {
        name: "Almighty Bot",
        description: "A Discord moderation bot built with Discord.js, MongoDB, and Express.js.",
        repoLink: "https://github.com/LakhindarPal/AlmightyBot",
        liveDemoLink: "https://discord.ly/almighty"
      },
      {
        name: "MyAnimeOdyssey",
        description: "An app for tracking watched and plan to watch anime.",
        repoLink: "https://github.com/LakhindarPal/MyAniJot",
        liveDemoLink: "https://myanijot.vercel.app"
      }
    ];

    const text = `<ol>
    ${projects.map(project => `
      <li>${project.name}:
      <p>${project.description}</p>
      <a href="${project.repoLink}">GitHub Repo</a>
      ${project.liveDemoLink ? `<a href="${project.liveDemoLink}">Live Demo</a>` : ''}
    </li>
   `).join("<br>")}
  </ol>`;

    print(text);
  }
}
