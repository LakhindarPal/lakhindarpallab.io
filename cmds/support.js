export default {
  name: "support",
  aliases: ["donate", "contribute"],
  description: "Support my work through contributions.",
  execute(print, subCmd, toTitleCase) {
    const platforms = {
      patreon: "https://patreon.com/LakhindarPal",
      kofi: "https://www.ko-fi.com/lakhindarpal",
      buymeacoffee: "https://www.buymeacoffee.com/lakhindarpal",
      liberapay: "https://liberapay.com/LakhindarPal/"
    };

    let text = `<p>Consider supporting me on:</p>`;

    if (subCmd) {
      const platform = platforms[subCmd];
      if (platform) {
        text += `<a href="${platform}">${toTitleCase(subCmd)}</a>`;
      } else {
        text = `<p>Invalid sub-command. Available sub-commands: ${Object.keys(platforms).join(', ')}</p>`;
      }
    } else {
      // Display all links
      text += `${Object.keys(platforms)
        .map(key => `<a href="${platforms[key]}">${toTitleCase(key)}</a>`)
        .join(" ")}`;
    }

    print(text);
  }
};