export default {
  name: "videos",
  aliases: ["video", "stream"],
  description: "Explore my video channels.",
  execute(print, subCmd, toTitleCase) {
    const platforms = {
      twitch: "https://twitch.tv/lakhindarpal",
      youtube: "https://youtube.com/c/yourchannel",
      tumblr: "https://www.tumblr.com/lakhindarpal",
      vimeo: "https://vimeo.com/lakhindarpal"
    };

    let text = `<p>Check out my video  content on:</p>`;

    if (subCmd) {
      const platform = platforms[subCmd];
      if (platform) {
        text += `<a href="${platform}">${toTitleCase(subCmd)}</a>`;
      } else {
        text = `<p>Invalid sub-command. Available sub-commands: ${Object.keys(platforms).join(', ')}</p>`;
      }
    } else {
      // Display all links
      text += `${Object.keys(platforms)
        .map(key => `<a href="${platforms[key]}">${toTitleCase(key)}</a>`)
        .join(" ")}`;
    }

    print(text);
  }
};