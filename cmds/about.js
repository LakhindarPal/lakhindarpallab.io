export default {
  name: "about",
  aliases: ["whoami"],
  description: "Some information about me.",
  execute(print) {
    print(
      `<p>Hello, I'm Lakhindar Pal, a self-taught web developer based in India. My journey in coding began in 2021, and I'm passionate about creating impactful web solutions. I'm deeply enamored with the world of open source. Always eager to learn and collaborate on interesting projects.</p>`
    )
  }
}