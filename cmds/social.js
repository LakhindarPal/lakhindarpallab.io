export default {
  name: "social",
  aliases: ["contact", "connect", "profiles"],
  description: "Connect with me on social media platforms.",
  execute(print, subCmd, toTitleCase) {
    const platforms = {
      linkedin: "https://linkedin.com/in/LakhindarPal",
      email: "mailto:dev.lakhindarpal@gmail.com",
      reddit: "https://reddit.com/user/LakhindarPal/",
      mastodon: "https://hachyderm.io/@LakhindarPal",
      discord: "https://discord.com/",
      facebook: "https://facebook.com/",
      instagram: "https://instagram.com/",
      twitter: "https://twitter.com/"
    };

    let text = `<p>Find me on social media:</p>`;

    if (subCmd) {
      const platform = platforms[subCmd];
      if (platform) {
        text += `<a href="${platform}">${toTitleCase(subCmd)}</a>`;
      } else {
        text = `<p>Invalid sub-command. Available sub-commands: ${Object.keys(platforms).join(', ')}</p>`;
      }
    } else {
      // Display all links
      text += `${Object.keys(platforms)
        .map(key => `<a href="${platforms[key]}">${toTitleCase(key)}</a>`)
        .join(" ")}`;
    }

    print(text);
  }
};