export default {
  name: "help",
  aliases: ["man", "h"],
  description: "Display a list of available commands.",
  execute(print, subCmd, _toTitleCase, commands, aliases) {
    let text = `<p>Command not found. Type 'help' for a list of available commands.</p>`;

    if (subCmd) {
      const cmdData = commands[subCmd] || commands[aliases[subCmd]];
      if (cmdData) text = `<p>${cmdData.name}</p>
      <p>- ${cmdData.description}</p>
      <p>Aliases: ${cmdData.aliases.join(", ")}</p>`;
    } else {
      const commandList = Object.keys(commands)
        .map(c => `<span>${c}</span>: ${commands[c].description}`)
        .join("<br>");

      text = `<p>Available commands:</p>
          <p>${commandList}</p>
          <p>Use 'help cmd-name' for more info on a specific command.</p>`
    }

    print(text);
  }
}