export default {
  name: "skills",
  aliases: ["skill", "expertise"],
  description: "Get insights into my technical expertise",
  execute(print, subCmd, toTitleCase) {
    const categories = {
      languages: ["HTML", "CSS", "JavaScript", "TypeScript", "Python", "C", "Kotlin"],
      frontend: ["React", "Bootstrap", "Tailwind CSS"],
      backend: ["Node.js", "Express", "Discord.js"],
      databases: ["MongoDB", "MySQL"],
      repos: ["GitHub", "GitLab", "Bitbucket"],
      hosting: ["Heroku", "AWS", "Netlify", "Varcel"],
      playgrounds: ["Glitch", "Replit, CodePen", "CodeSandBox"],
      editors: ["VS Code"]
    };

    const descriptions = {
      languages: "Proficient in programming languages like",
      frontend: "Experienced in Frontend technologies such as",
      backend: "Skilled in Backend development using",
      databases: "Well-versed in Database management with",
      repos: "Proficient in Version control using",
      hosting: "Familiar with cloud hosting services like",
      playgrounds: "Experience with online coding platforms like",
      editors: "Familiar with various code editors, including"
    };

    let text = "";

    if (subCmd) {
      const category = categories[subCmd];
      if (category) {
        text = `<p>${descriptions[subCmd]}: ${category.join(", ")}</p>`;
      } else {
        text = `<p>Invalid sub-command. Available sub-commands: ${Object.keys(categories).join(', ')}</p>`;
      }
    } else {
      // Display all skills
      text = "<p>My technical skills and knowledge</p><ul>";
      Object.keys(categories).forEach(category => {
        text += `<li>${toTitleCase(category)}: ${categories[category].join(', ')}</li>`;
      });
      text += "</ul>";
    }

    print(text);
  }
};