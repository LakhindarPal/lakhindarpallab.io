export default {
  name: "blogs",
  aliases: ["articles", "blog", "article"],
  description: "Checkout latest articles from my blog.",
  execute(print, subCmd, toTitleCase) {
    const platforms = {
      devto: "https://dev.to/lakhindarpal",
      medium: "https://lakhindarpal.medium.com",
      substack: "https://lakhindarpal.substack.com",
      hashnode: "https://lakhindarpal.hashnode.dev"
    }

    let text = `<p>Read my articles on:</p>`;

    if (subCmd) {
      const platform = platforms[subCmd];
      if (platform) {
        text += `<a href="${platform}">${toTitleCase(subCmd)}</a>`;
      } else {
        text = `<p>Invalid sub-command. Available sub-commands: ${Object.keys(platforms).join(', ')}</p>`;
      }
    } else {
      // Display all links
      text += `${Object.keys(platforms)
        .map(key => `<a href="${platforms[key]}">${toTitleCase(key)}</a>`)
        .join(" ")}`;
    }

    print(text);
  }
}