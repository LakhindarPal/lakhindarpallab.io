const terminalInput = document.getElementById("input");
const terminalOutput = document.getElementById("output");

const cmdNames = ["help", "about", "blogs", "clear", "portfolio", "repos", "skills", "social", "support", "videos"];

const commands = {};
const aliases = {};
const history = [];

await Promise.all(cmdNames.map(async (name) => {
  const { default: command } = await import(`./cmds/${name}.js`);

  commands[command.name] = command;
}));

for (const cmd in commands) {
  const cmdObj = commands[cmd];
  if (cmdObj.aliases) {
    cmdObj.aliases.forEach(alias => {
      aliases[alias] = cmd;
    });
  }
}

function print(text) {
  terminalOutput.innerHTML += text;
  terminalOutput.scrollTop = terminalOutput.scrollHeight;
}

function toTitleCase(str) {
  return str.replace(/\b\w/g, match => match.toUpperCase());
}

terminalInput.addEventListener("keydown", function (e) {
  e.preventDefault();

  switch (e.key) {
    case "Enter":
      const argument = terminalInput.value?.trim();
      terminalInput.value = "";
      history.push(argument);

      const [cmd, subCmd] = argument.toLowerCase().split(" ");
      const command = commands[cmd] || commands[aliases[cmd]];

      print(`<p>$ ${argument}</p>`);

      if (command) {
        if (command.name === "clear") {
          terminalOutput.textContent = "";
        } else {
          command.execute(print, subCmd, toTitleCase, commands, aliases);
        }
      } else {
        print(`<p>Command not found. Type 'help' for a list of available commands.</p>`);
      }
      break;

    case "ArrowUp":
      terminalInput.value = history[history.length - 1];
      break;

    case "ArrowDown":
      terminalInput.value = "";
      break;
  }
});