function changeTheme(dot) {
  document.querySelectorAll(".theme li").forEach((item) => {
    item.classList.remove("active-theme");
  });

  const bgColor = window.getComputedStyle(dot).backgroundColor;

  document.documentElement.style.setProperty("--main-color", bgColor);

  dot.classList.add("active-theme");
}